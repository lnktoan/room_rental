-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 03, 2019 at 08:48 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `room_admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_bin NOT NULL,
  `password` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'thanh', '123'),
(2, 'tai', '123'),
(3, 'toan', '123'),
(4, 'minh', '123'),
(5, 'bao', '123'),
(6, 'binh', '123'),
(7, 'cong', '123');

-- --------------------------------------------------------

--
-- Table structure for table `room_rental`
--

CREATE TABLE `room_rental` (
  `id` int(11) NOT NULL,
  `name` varchar(500) COLLATE utf8_bin NOT NULL,
  `price` float NOT NULL,
  `image` text COLLATE utf8_bin NOT NULL,
  `location` varchar(500) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `room_rental`
--

INSERT INTO `room_rental` (`id`, `name`, `price`, `image`, `location`, `description`, `admin_id`) VALUES
(1, 'An nhien homestay', 90, 'images/img_1.jpg', 'Quan 1', 'Nha dep, co cho du', 1),
(2, 'Ton Duc Thang homestay', 100, 'images/img_2.jpg', 'Quan 2', 'Nha dep, rong', 2),
(3, 'Vung Tau hometsay', 200, 'images/img_3.jpg', 'Quan 3', 'Nha co cho giu, cam vao', 3),
(4, 'Bee homestay', 190, 'images/hero_bg_1.jpg', 'Quan 1', 'Nha dep, co cho, view thanh pho', 4),
(5, 'LyLy homestay', 120, 'images/nhoo.jpg', 'Quan 2', 'Nha hien dai, rong, thoang mat, hop ve sinh', 5),
(6, 'Boss hometsay', 220, 'images/_DSF8938.jpg', 'Quan 3', 'Nha sang chanh, tien ich, ngau loi ', 6),
(7, 'Luxury homestay', 300, 'images/img_7.jpg', 'Quan 1', 'Noi that sang trong, nam o trung tam tp, day du tien ich, hien dai', 6),
(8, 'Running homestay', 140, 'images/nho.jpg', 'Quan 2', 'Nha sach se, rong, moi truong tuoi xanh, chay nhay thoai mai', 4),
(9, 'Theme hometsay', 200, 'images/nn.jpg', 'Quan 3', 'Nha to bu, thoai mai va co hang xom vui nhon nhip', 7),
(10, 'Love homestay', 125, 'images/vua.jpg', 'Quan 2', 'Nha sach se, rong, moi truong tuoi xanh, chay nhay thoai mai', 4),
(11, 'Luxubu homestay', 110, 'images/img_4.jpg', 'Quan 6', 'Nha rong, view song nuoc, moi truong tuoi xanh', 4),
(12, 'Yolo homestay', 115, 'images/bb.jpg', 'Quan 4', 'Nha gon gang, thoang, hang xom vui ve khong quao quo', 2),
(13, 'Wakanda Forever homestay', 260, 'images/the-an-homestay.jpg', 'Quan 7', 'Nha hien dai, day du noi that, gan truong DH Ton Duc Thang', 5),
(14, 'Kun homestay', 280, 'images/lac.jpg', 'Quan 3', 'Nha dep, rong, moi truong tuoi xanh, giao thong thuan loi, kien truc nhat ban', 3),
(15, 'Zoom homestay', 230, 'images/img_8.jpg', 'Quan 6', 'Nha sach se, rong, co ham gui xe, moi truong tien ich', 6),
(16, 'Zara homestay', 130, 'images/sandy-homestay-da-nang-4-4.jpg', 'Quan 5', 'Nha rong va thoang, moi truong nhieu cay xanh,co ban cong', 4),
(18, 'King homestay', 290, 'images/urban_house_1.jpg', 'Quan 7', 'Nha co noi that thong minh, smart home, view dep, co bon tam bu nha, moi truong song linh dong', 4);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `phone` varchar(100) COLLATE utf8_bin NOT NULL,
  `mail` varchar(100) COLLATE utf8_bin NOT NULL,
  `room_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `phone`, `mail`, `room_id`) VALUES
(4, 'Nguyen Cong Thanh', '0903035175', 'ncthanh0411@gmail.com', 2),
(5, 'Nguyen Cong Thanh', '0903053132', 'ncthanh0411@gmail.com', 1),
(6, 'Nguyen Tan Tai', '0903053132', 'taitai@gmail.com', 1),
(7, 'Le Tuan Minh', '0902028274', 'leTuanMinh@gmail.com', 13),
(8, 'Nguyen Cong Thanh2', '0903053132', '2', 13),
(9, 'Nguyen Cong Thanh23', '0903053132', 'ncthanh0411@gmail.com', 13),
(10, 'Nguyen Cong Thanh2', '11', 'leTuanMinh@gmail.com', 13),
(11, 'Tran Thanh Binh', '0903053132', 'leTuanMinh@gmail.com', 13);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_rental`
--
ALTER TABLE `room_rental`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_id` (`admin_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_id` (`room_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `room_rental`
--
ALTER TABLE `room_rental`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `room_rental`
--
ALTER TABLE `room_rental`
  ADD CONSTRAINT `room_rental_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`id`);

--
-- Constraints for table `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `student_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `room_rental` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
